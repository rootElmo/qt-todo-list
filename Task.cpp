#include "Task.h"
#include "ui_Task.h"

#include <QInputDialog>
#include <QDebug>

Task::Task(const QString& name, QWidget *parent) :
        QWidget(parent),
        ui(new Ui::Task)
{
    ui->setupUi(this);
    setName(name);

    connect(ui->editButton, &QPushButton::clicked, this, &Task::rename);
    connect(ui->descEditBtn, &QPushButton::clicked, this, &Task::edit_desc);
    connect(ui->removeButton, &QPushButton::clicked, [this] {
        emit removed(this);
    });
    connect(ui->checkbox, &QCheckBox::toggled, this, &Task::checked);
}

Task::~Task()
{
    qDebug() << "~Task() called";
    delete ui;
}

void Task::setName(const QString& name)
{
    ui->checkbox->setText(name);
}

QString Task::name() const
{
    return ui->checkbox->text();
}

bool Task::isCompleted() const
{
   return ui->checkbox->isChecked();
}

void Task::rename()
{
    bool ok;
    QString value = QInputDialog::getText(this, tr("Edit task"),
                                          tr("Task name"), QLineEdit::Normal,
                                          this->name(), &ok);
    if (ok && !value.isEmpty()) {
        setName(value);
    }
}

/*-------DESCRIPTION------*/

QString Task::get_description() const {
    return _description;
}

void Task::set_description(const QString &desc) {
    _description = desc;
}

void Task::edit_desc() {
    bool ok;

    QString desc = QInputDialog::getMultiLineText(this, tr("Edit Task Description"),
                                                  tr("Task Description"),
                                                  this->get_description(), &ok);
    if (ok && !desc.isEmpty()) {
        set_description(desc);
    }
}

/*-------!DESCRIPTION------*/

void Task::checked(bool checked)
{
    QFont font(ui->checkbox->font());
    font.setStrikeOut(checked);
    ui->checkbox->setFont(font);

    emit statusChanged(this);
}
