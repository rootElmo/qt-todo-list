#ifndef TASK_H
#define TASK_H

#include <QWidget>
#include <QString>

namespace Ui {
class Task;
}

class Task : public QWidget
{
    Q_OBJECT

public:
    explicit Task(const QString& name, QWidget *parent = 0);
    ~Task();

    void setName(const QString& name);
    QString name() const;

    QString get_description() const;
    void set_description(const QString &desc);

    bool isCompleted() const;

public slots:
    void rename();
    void edit_desc();

signals:
    void removed(Task* task);
    void statusChanged(Task* task);

private slots:
    void checked(bool checked);

private:
    Ui::Task *ui;
    QString _description = "";
};

#endif // TASK_H
